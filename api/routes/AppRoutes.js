'use strict';
module.exports = function (app) {
    var controller = require('../controllers/TransactionsController');

    app.route('/transactions')
        .get(controller.data)
        .post(controller.data)

    app.route('/summary')
        .get(controller.summary)
        .post(controller.summary)
};
