var axios = require('axios');
var data = require('../resources/transactions.json')
var summary = require('../resources/summary.json')

exports.summary = async (req, res) => {
    res.json(summary);
}

exports.data = async (req, res) => {
    res.json(data);
}
